﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallPool : MonoBehaviour {

    public GameObject wallPrefab;                                 
    public int wallPoolSize = 5;                                  
    public float spawnRate = 3f;                                  
    public float wallMin = -1f;                             
    public float wallMax = 3.5f;                           

    private GameObject[] walls;                              
    private int currentWall = 0;                              

    private Vector2 objectPoolPosition = new Vector2(-15, -25);    
    private float spawnXPosition = 10f;

    private float timeSinceLastSpawned;


    void Start()
    {
        timeSinceLastSpawned = 0f;

        walls = new GameObject[wallPoolSize];
        for (int i = 0; i < wallPoolSize; i++)
        {
            walls[i] = (GameObject)Instantiate(wallPrefab, objectPoolPosition, Quaternion.identity);
        }
    }

    void Update()
    {
        timeSinceLastSpawned += Time.deltaTime;

        if (GameControl.instance.gameOver == false && timeSinceLastSpawned >= spawnRate)
        {
            timeSinceLastSpawned = 0f;

            float spawnYPosition = Random.Range(wallMin, wallMax);

            walls[currentWall].transform.position = new Vector2(spawnXPosition, spawnYPosition);

            currentWall++;

            if (currentWall >= wallPoolSize)
            {
                currentWall = 0;
            }
        }
    }
}
